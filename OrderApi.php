<?php
use Bitrix\Main\Loader;

class OrderApi
{

    private $deliveryId = 2;

    public function __construct($deliveryId = 2)
    {
        Loader::includeModule('sale');
        require_once ($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/classes/FormApi.php');
        require_once ($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/classes/UserApi.php');
        require_once ($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/classes/DeliveryApi.php');
        $this->deliveryId = $deliveryId;
    }

    /**
     * @param array $params - массив данных заказа
     * @return array
     * Функция возвращают данные по новому заказу или ошибки исполнения
     */
    public function makeOrder(array $params = []): array
    {

        if ($errors = $this->_validateParams($params)) {
            return ['success' => false, 'error' => ['text' => implode(', ', $errors)]];
        }

        $userData = UserApi::getUserToOrder($params);

        $basket = Bitrix\Sale\Basket::loadItemsForFUser(Bitrix\Sale\Fuser::getId(), 's1');
        $basketItems = $basket->getBasketItems();
        if (empty($basketItems)) {
            return ['success' => false, 'error' => ['text' => 'Ваша корзина пуста']];
        }
        $basketStringData = [];
        foreach ($basketItems as $basketItem) {
            $basketStringData[] =  $basketItem->getField('NAME') . ' - в количестве ' . $basketItem->getQuantity() . ' шт. Полная стоимость - ' . $basketItem->getFinalPrice() .' руб.';
        }
        $basketStringData[] = 'Итоговая стоимость заказа - ' . $basket->getPrice() . ' руб.';

        $userId = $userData['userId'] ?: Bitrix\Sale\Fuser::getId();
        $order = Bitrix\Sale\Order::create(\Bitrix\Main\Context::getCurrent()->getSite(), $userId);
        $order->setPersonTypeId(1); // 1 - ID типа плательщика
        $order->setBasket($basket);


        $shipmentCollection = $order->getShipmentCollection();
        $shipmentCollection->createItem(
            Bitrix\Sale\Delivery\Services\Manager::getObjectById($this->deliveryId) // ID службы доставки
        );

        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem(
            Bitrix\Sale\PaySystem\Manager::getObjectById(1) // ID платежной системы
        );
        $order->doFinalAction(true);

        $this->setOrderProps(['orderParams' => $params, 'order' => $order]);

        $price = $order->getPrice();
        $payment->setField("SUM", $price);
        $payment->setField("CURRENCY", $order->getCurrency());
        $r = $order->save();
        if (!$r->isSuccess()) {
            return ['success' => false, 'error' => ['text' => $r->getErrorMessages()]];
        }

        $params['order'] = $order->getId();
        $params['basketItems'] = implode('<br>', $basketStringData);
        $params['delivery'] = (new DeliveryApi())->getAll()[$this->deliveryId]['label'];
        (new FormApi('NEW_ORDER'))->addOrder($params);
        (new FormApi('NEW_ORDER_USER'))->addOrder($params);

        return ['success' => true, 'href' => '/order/' . $order->getId() . '/'];
    }

    /**
     * @param array $params Массив данных заказа
     *          $params =>[
     *             'name'=>(string) Имя
     *             'email'=>(string) Email
     *             'phone'=>(string) Телефон
     *             'deliveryDate'=>(string) Дата доставки
     * ]
     * @return array
     */
    private function getOrderProps(array $params = [])
    {
        return [
            ['ID' => 1, 'NAME' => 'Представьтесь', 'CODE' => 'NAME', 'VALUE' => $params['name']],
            ['ID' => 2, 'NAME' => 'Телефон', 'CODE' => 'PHONE', 'VALUE' => $params['phone']],
            ['ID' => 3, 'NAME' => 'Электронная почта', 'CODE' => 'EMAIL', 'VALUE' => $params['email']],
            ['ID' => 4, 'NAME' => 'Город и адрес доставки', 'CODE' => 'ADDRESS', 'VALUE' => $params['address']],
            ['ID' => 5, 'NAME' => 'Название организации', 'CODE' => 'ORGANIZATION', 'VALUE' => $params['organization']],
        ];
    }

    /**
     * @param array $params массив входных данных
     *          $params => [
     *          'order' => (order) объект заказа
     *          'orderParams' => (array) массив параметров заказа
     *              [
     *                  'name'=>(string) Имя
     *                  'email'=>(string) Email
     *                  'phone'=>(string) Телефон
     *                  'comment'=>(string) Комментарий
     *              ]
     * ]
     */
    private function setOrderProps(array $params = [])
    {
        $orderProps = $this->getOrderProps($params['orderParams']);
        $orderCollection = $params['order']->getPropertyCollection();
        foreach ($orderProps as $orderProp) {
            $arItem["VALUE"] = htmlspecialchars($orderProp["VALUE"]);
            $propertyValue = $orderCollection->getItemByOrderPropertyId($orderProp["ID"]);
            $propertyValue->setField('VALUE', $orderProp["VALUE"]);
        }
    }

    /**
     * @param array $params массив данных заказа
     *          $params =>[
     *              'phone' => (string|int) телефон. Проверяемое поле
     *              'email' => (string) email. Проверяемое поле
     * ]
     * @return array
     */
    private function _validateParams(array $params = [])
    {

        foreach ($params as &$param) {
            $param = strip_tags(trim($param));
        }
        unset($param);
        $errors = [];
        if (!$params['phone']) {
            $errors['phone'] = 'Отсутствует номер телефона';
        }

        if ($params['email'] && !filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = 'Email не прошел валидацию';
        }

        return $errors;
    }

    public function checkOrder(int $orderId): bool
    {

        $dbBasketItems = CSaleOrder::GetList(
            ["DATE_INSERT" => "ASC"], ['ID' => $orderId],
            false, false, ["ID", "MODULE"]
        );
        if ($dbBasketItems->Fetch()) {
            return true;
        }

        return false;
    }

}