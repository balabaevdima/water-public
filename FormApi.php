<?php

use \Bitrix\Main\Mail\Event;
use \Bitrix\Main\Loader;

class FormApi
{

    private string $eventMailName;
    private array $mailBody = [];
    private array $errorsMessage = [];
    private array $elementData = [];
    private array $propertyData = [];
    private int $fileId = 0;

    /**
     * CallBackApi constructor.
     * @param string $eventMailName
     */
    public function __construct(string $eventMailName)
    {
        $this->eventMailName = $eventMailName;
        include_once ($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/classes/HLWorker.php');
    }

    /**
     * @param array $params
     * @return array
     */
    public function addOrder(array $params, $file = [],  array $requiredFields = []): array
    {
        $result['test'] = $file;
        $this->_validateParams($params, $requiredFields);
        if($file){
            $this->_saveFile($file);
        }

        if ($this->errorsMessage) {
            return ['success' => false, 'errors' => $this->errorsMessage];
        }
        $this->mailBody = array_merge($this->elementData, $this->propertyData);
        $this->_validateMailBody();

        $result['success'] = $this->_sendMail();
        return $result;
    }

    private function _saveFile($file)
    {
        $this->fileId = CFile::SaveFile($file['spec-file'],"mailatt");
        if($this->fileId){
            (new HLWorker('Files'))->add(['props'=>['UF_FILE'=>$this->fileId]]);
        }
    }

    private function _validateMailBody()
    {
        if ($this->mailBody['PREVIEW_TEXT']) {
            $this->mailBody['DESCRIPTION'] = $this->mailBody['PREVIEW_TEXT'];
            unset($this->mailBody['PREVIEW_TEXT']);
        }
    }


    /**
     * @param array $params
     *          $params = [
     *              'mailBody' => (array) Массив данных для почтовых шаблонов
     *              'eventMailName' => (string) тип почтового события
     * ]
     * @return bool
     */
    private function _sendMail(array $params = []): bool
    {

        $eventMailName = $params['eventMailName'] ?: $this->eventMailName;
        $mailBody = $params['mailBody'] ?: $this->mailBody;

        $resultObj = Event::send([
            'EVENT_NAME' => $eventMailName,
            'LID' => 's1',
            'C_FIELDS' => $mailBody,
            'DUPLICATE' => 'N',
            "FILE" => $this->fileId ? [$this->fileId] : []
        ]);

        return (bool)$resultObj->getId();

    }

    /**
     * Валидация параметров. Запоминает данные для письма и ошибки
     * @param array $params
     */
    private function _validateParams(array $params, array $requiredFields = [])
    {

        foreach ($params as &$param) {
            $param = trim($param);
        }
        unset($param);

        foreach ($requiredFields as $requiredField) {
            if (!$params[$requiredField]) {
                $this->errorsMessage[$requiredField] = "Обязательное поле {$requiredField} пустое";
            }
        }

        if ($params['name']) {
            $this->_validateName((string)$params['name']);
        }

        if ($params['phone']) {
            $this->_validatePhone((string)$params['phone']);
        }

        if ($params['email']) {
            $this->_validateEmail((string)$params['email']);
        }

        if ($params['message']) {
            $this->_validateComment((string)$params['message']);
        }

        if ($params['organization']) {
            $this->_validateOrganization((string)$params['organization']);
        }
        if ($params['order']) {
            $this->_validateOrder((int)$params['order']);
        }
        if ($params['address']) {
            $this->_validateAddress((string)$params['address']);
        }
        if ($params['delivery']) {
            $this->_validateDelivery((string)$params['delivery']);
        }
        if ($params['basketItems']) {
            $this->_validateBasketItems((string)$params['basketItems']);
        }

    }


    /**
     * @param string $name
     */
    private function _validateName(string $name)
    {
        $this->elementData["NAME"] = htmlspecialcharsEx(trim($name, '"\''));
    }


    /**
     * @param string $email
     */
    private function _validateEmail(string $email)
    {
        if (!preg_match('/.+@.+\..+/i', $email, $matches)) {
            $this->errorsMessage['email'] = "Не правильно задан E-mail";
            return;
        }
        $this->propertyData["EMAIL"] = htmlspecialcharsEx($email);
    }

    /**
     * @param string $comment
     */
    private function _validateComment(string $comment)
    {
        $this->elementData["PREVIEW_TEXT"] = htmlspecialcharsEx(trim($comment, '"\''));
    }

    /**
     * @param string $string
     */
    private function _validateOrganization(string $string)
    {
        $this->propertyData["ORGANIZATION"] = htmlspecialcharsEx(trim($string, '"\''));
    }
    /**
     * @param int $order
     */
    private function _validateOrder(int $order)
    {
        $this->propertyData["ORDER"] = $order;
    }
    /**
     * @param string $string
     */
    private function _validateAddress(string $string)
    {
        $this->propertyData["ADDRESS"] = htmlspecialcharsEx(trim($string, '"\''));
    }

    /**
     * @param string $string
     */
    private function _validateDelivery(string $string)
    {
        $this->propertyData["DELIVERY"] = htmlspecialcharsEx(trim($string, '"\''));
    }

    /**
     * @param string $string
     */
    private function _validateBasketItems(string $string)
    {
        $this->propertyData["BASKET_ITEMS"] = trim($string, '"\'');
    }

    /**
     * @param string $phone
     */
    private function _validatePhone(string $phone)
    {
        if (preg_match('/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/', trim($phone, '"\''), $matches)) {
            $this->propertyData["PHONE"] = htmlspecialcharsEx($phone);
            return;
        }
        $this->errorsMessage['phone'] = "Неправильно задан номер телефона";
    }

}